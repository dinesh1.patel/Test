@file:Suppress("unused")

package com.xerini.interview.server.api

import java.util.concurrent.CopyOnWriteArrayList
import javax.ws.rs.*
import javax.ws.rs.core.MediaType.APPLICATION_JSON
import javax.ws.rs.core.Response

@Path("/geo-json")
class GeoJsonService {
    private val allCoordinates = CopyOnWriteArrayList<List<Double>>()
	private val singleCoordinate = ArrayList<Double>()

    @GET
    @Produces(APPLICATION_JSON)
    fun getGeoJson(): GeoJsonObject {
        var cords1 = ArrayList<Double>()
        cords1.add(112.22)
        cords1.add(133.2)
        allCoordinates.add(cords1)
        var cords2 = ArrayList<Double>()
        cords1.add(100.22)
        cords1.add(107.2)
        allCoordinates.add(cords1)

        var geoFeatures = ArrayList<GeoJsonFeature>()
        var geoData = GeometryData(singleCoordinate)
        var geoFeature = GeoJsonFeature(geoData)
        geoFeatures.add(geoFeature)
        return GeoJsonObject(geoFeatures)
    }

	@GET
    @Path("/din")
    @Produces(APPLICATION_JSON)
    fun seyHello(): String {
        return "Hello World";
    }
	
    @Path("/add")
    @POST
    @Consumes(APPLICATION_JSON)
    fun addPoint(coordinates: List<Double>): Response {
        allCoordinates.add(coordinates) // add coordinate pairs to list of list of doubles
        File("C:/Temp/allcoordinates.txt").bufferedWriter().use { out -> out.write(allCoordinates.toString()) }

    }
	
    fun getDestinations(): List<Double> {
		val cords = ArrayList<Double>()
		cords.add(112.22)
		cords.add(133.2)
		val geoFeatures = ArrayList<GeoJsonFeature>()
		val geoData = GeometryData(cords)
		val geoFeature = GeoJsonFeature(geoData)
		geoFeatures.add(geoFeature)
		val geoFinal = GeoJsonObject(geoFeatures)

		return cords
    }
}

data class GeoJsonObject(val features: List<GeoJsonFeature>) {
    val type: String = "FeatureCollection"
}

data class GeoJsonFeature(val geometry: GeometryData?, val properties: Map<String, Any?> = emptyMap()) {
    val type: String = "Feature"
}

data class GeometryData(val coordinates: List<Double>) {
    val type: String = "Point"
}