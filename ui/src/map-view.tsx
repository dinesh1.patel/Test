import * as React from "react"
import {useEffect, useState} from "react"
import {Feature, Map, MapBrowserEvent, View} from "ol";
import {fromLonLat} from "ol/proj";
import TileLayer from "ol/layer/Tile";
import {OSM, Vector} from "ol/source";

import "ol/ol.css";
import {FeatureLike} from "ol/Feature";
import VectorLayer from "ol/layer/Vector";
import {GeoJSON} from "ol/format";
import {Icon, Style} from "ol/style";

const geoJson = new GeoJSON()

const mapPinStyle = new Style({
    image: new Icon({
        src: "/img/map-pin-blue.png",
        scale: 25 / 50,
        anchor: [0.5, 1.0]
    })
});

export const MapView: React.FC = () => {
    const [map, setMap] = useState<Map | undefined>(undefined)
    const [featureLayer, setFeatureLayer] = useState<VectorLayer | undefined>()
    const [features, setFeatures] = useState<FeatureLike[]>([])

    useEffect(() => {
        const map = new Map({
            target: "map",
            layers: [
                new TileLayer({
                    source: new OSM()
                })
            ],
            view: new View({
                center: fromLonLat([-0.023758, 51.547504]),
                zoom: 13,
                minZoom: 6,
                maxZoom: 18
            })
        })
        map.on("click", onMapClick);

        setMap(map)
        loadFeatureData()
    }, [])

    useEffect(() => {
        if (map) {
            setFeatureLayer(addFeatureLayer(featureLayer, features))
        }
    }, [map, features])

    const loadFeatureData = () => {
        fetch("/api/geo-json")
            .then(response => response.json())
            .then(json => setFeatures(geoJson.readFeatures(json)))
    }

	const postFeatureData = (data) => {
        fetch("/api/geo-json/add", {
                method: 'post',
                body: JSON.stringify(data)
              }).then(function(response) {
                return response.json();
              }).then(function(data) {

              });
    }
	
    const addFeatureLayer = (previousLayer: VectorLayer, features: FeatureLike[]): VectorLayer => {
        const newLayer = previousLayer ? previousLayer : new VectorLayer({
            style: mapPinStyle
        });

        if (previousLayer != undefined) {
            previousLayer.getSource().clear();
        } else {
            map.addLayer(newLayer);
        }

        (newLayer as any).tag = "features";

        const source = new Vector({
            format: geoJson,
            features: features as Feature<any>[]
        });

        newLayer.setSource(source);

        return newLayer
    }

    const onMapClick = (e: MapBrowserEvent) => {
        var clickedCords = map.getLonLatFromViewPortPx(e.xy);
        /*
		const map = new Map({
            target: "map",
            layers: [
                new TileLayer({
                    source: new OSM()
                })
            ],
            view: new View({
                center: fromLonLat([clickedCords.lat, clickedCords.lon]),
                zoom: 19,
                minZoom: 10,
                maxZoom: 18
            })
        })
		*/
		//Save the coordinates via a post call to the backend
		postFeatureData(clickedCords)
       
		//Call Get to fetch / reload / refresh the UI
		//the 2 calls to useEffect (componentHasLoaded), need to be recalled
		//first call is placing a static point, which should get replaced with a point from the 
		//back end GET call (if data exists)
		//In theory this should invoke it all as a refresh operation
		window.location.reload(false);
    }

    return <div>
        <div id="map" style={{height: "500px", width: "500px"}}/>
    </div>
}